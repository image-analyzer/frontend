FROM node:lts-alpine

RUN mkdir -p /usr/src/frontend
WORKDIR /usr/src/frontend

COPY . .

RUN npm install
RUN npm run build

EXPOSE 3000

CMD [ "npm", "run", "start" ]