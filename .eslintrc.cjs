module.exports = {
  env: {
    browser: true,
    es2021: true,
    jest: true
  },
  extends: [
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'airbnb',
    'airbnb/hooks',
    'prettier'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: { jsx: true },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['react', 'react-refresh'],
  rules: {
    'react-refresh/only-export-components': [
      'warn',
      { allowConstantExport: true },
    ],
    'react/jsx-indent': [2, 2],
    'react/jsx-indent-props': [2, 2],
    'react/jsx-filename-extension': [
      1,
      { extensions: ['.js', '.jsx', '.tsx'] }
    ],
    'react/jsx-props-no-spreading': 'off',
    'react/require-default-props': 'off',
    'import/no-unresolved': 'off',
    'import/prefer-default-export': 'off',
    'no-unused-vars': 'off',
    '@typescript-eslint/no-unused-vars': ['warn'],
    'react/react-in-jsx-scope': 'off',
    'react/function-component-definition': 'off',
    'no-shadow': 'off',
    'comma-dangle': ['error', 'never'],
    semi: ['error', 'never'],
    'eol-last': 'off',
    'no-underscore-dangle': 'off',
    'import/extensions': 'off',
    'import/no-extraneous-dependencies': 'off',
    'jsx-quotes': ['error', 'prefer-single'],
    'max-len': [
      'error',
      {
        code: 120,
        ignoreComments: true,
        ignoreUrls: true,
        ignoreTemplateLiterals: true,
        ignorePattern: '(d=("|\')([\\s\\S]*?)("|\')|^^import .*)'
      }
    ],
    'function-paren-newline': 'off',
    'no-trailing-spaces': [
      'warn',
      { skipBlankLines: true, ignoreComments: true }
    ],
    'arrow-parens': ['error', 'as-needed'],
    'no-confusing-arrow': 'off',
    'object-curly-newline': 'off',
    'react/no-unstable-nested-components': 'off',
    'react/prop-types': 'off',
    'no-param-reassign': 'off',
    'react/no-unescaped-entities': 'off',
    'operator-linebreak': 'off',
    radix: 'off',
    'jsx-a11y/label-has-associated-control': 'off',
    'no-plusplus': 'off',
    'react/jsx-one-expression-per-line': 'off',
    camelcase: 'off'
  },
  globals: {
    __IS_DEV__: true,
    __API_URL__: true,
    __VERSION__: true,
    JSX: 'readonly'
  },
  ignorePatterns: ['**/src/**/*.json']
}
