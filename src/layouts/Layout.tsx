import { ReactNode } from 'react'

import { Box, Flex, Heading, Image } from '@chakra-ui/react'

import PolytechLogo from 'assets/images/polytech.png'

interface LayoutProps {
  children: ReactNode
}

export const Layout = ({ children }: LayoutProps) => (
  <Flex direction='column' py={10} minH='100vh'>
    <Image src={PolytechLogo} alt='Polytech Logo' w='180px' h='auto' />
    <Flex direction='column' alignItems='center' mt={8} px={5}>
      <Heading fontWeight={500} mb={10}>
        Исследование данных из файлов с изображениями
      </Heading>
      <Box w='100%' maxW='800px'>
        {children}
      </Box>
    </Flex>
  </Flex>
)
