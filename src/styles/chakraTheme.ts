import { extendTheme } from '@chakra-ui/react'

const baseFont = 'Nunito'

/**
 * Конфигурация Chakra UI
 */
export const chakraTheme = extendTheme({
  config: {
    initialColorMode: 'dark',
    useSystemColorMode: false
  },
  fonts: {
    heading: `${baseFont}, sans-serif`,
    body: `${baseFont}, sans-serif`
  },
  styles: {
    global: {
      body: {
        minH: '100vh'
      },
      ol: {
        marginInlineStart: '1em'
      },
      ul: {
        marginInlineStart: '1em'
      }
    }
  },
  breakpoints: {
    sm: '577px',
    md: '769px',
    lg: '993px',
    xl: '1281px',
    '2xl': '1537px'
  }
})
