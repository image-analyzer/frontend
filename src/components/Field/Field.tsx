import { ReactNode } from 'react'

import { Stack, Text } from '@chakra-ui/react'

interface FieldProps {
  label: ReactNode
  children: ReactNode
}

export const Field = ({ label, children }: FieldProps) => (
  <Stack spacing={1}>
    <Text color='gray.400' fontWeight={700} mb={1}>
      {label}:
    </Text>
    {children}
  </Stack>
)
