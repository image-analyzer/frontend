import { Icon } from '@chakra-ui/icons'
import { IconProps } from '@chakra-ui/react'

export const UploadIcon = (props: IconProps) => (
  <Icon viewBox='0 0 64 64' {...props} fill='none'>
    <g>
      <path
        d='M57.47 38.6a2 2 0 0 0-2 2v6.83a6.07 6.07 0 0 1-6.07 6.07H14.6a6.07 6.07 0 0 1-6.07-6.07V40.6a2 2 0 1 0-4 0v6.83A10.08 10.08 0 0 0 14.6 57.5h34.8a10.08 10.08 0 0 0 10.07-10.07V40.6a2 2 0 0 0-2-2z'
        fill='currentColor'
      />
      <path
        d='m22 21.3 8-8V43a2 2 0 0 0 4 0V13.33l8 8a2 2 0 0 0 2.83-2.83L33.42 7.09a2.16 2.16 0 0 0-1-.55 2.06 2.06 0 0 0-1.81.55L19.2 18.47A2 2 0 0 0 22 21.3z'
        fill='currentColor'
      />
    </g>
  </Icon>
)
