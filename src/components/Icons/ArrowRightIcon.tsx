import { Icon } from '@chakra-ui/icons'
import { IconProps } from '@chakra-ui/react'

export const ArrowRightIcon = (props: IconProps) => (
  <Icon viewBox='0 0 32 32' {...props} fill='none'>
    <g>
      <path
        d='m28.71 15.29-6-6a1 1 0 0 0-1.42 1.42l4.3 4.29H4a1 1 0 0 0 0 2h21.59l-4.3 4.29a1 1 0 0 0 0 1.42 1 1 0 0 0 1.42 0l6-6a1 1 0 0 0 .21-.33 1 1 0 0 0 0-.76 1 1 0 0 0-.21-.33z'
        fill='currentColor'
      />
    </g>
  </Icon>
)
