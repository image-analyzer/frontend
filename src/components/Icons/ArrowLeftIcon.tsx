import { Icon } from '@chakra-ui/icons'
import { IconProps } from '@chakra-ui/react'

export const ArrowLeftIcon = (props: IconProps) => (
  <Icon viewBox='0 0 32 32' {...props} fill='none'>
    <g>
      <path
        d='M28 15H6.41l4.3-4.29a1 1 0 0 0-1.42-1.42l-6 6a1 1 0 0 0-.21.33 1 1 0 0 0 0 .76 1 1 0 0 0 .21.33l6 6a1 1 0 0 0 1.42 0 1 1 0 0 0 0-1.42L6.41 17H28a1 1 0 0 0 0-2z'
        fill='currentColor'
      />
    </g>
  </Icon>
)
