import { Button, Flex, Image, Text } from '@chakra-ui/react'

import { formatBytes } from 'utils/formatBytes'

interface FilePreviewProps {
  file: File
  removeFile?: () => void
  small?: boolean
  noDelete?: boolean
}

export const FilePreview = ({
  file,
  removeFile,
  small,
  noDelete
}: FilePreviewProps) => (
  <Flex gap={small ? 2 : 4} alignItems='center'>
    <Image
      src={URL.createObjectURL(file)}
      alt={file.name}
      w={small ? 12 : 14}
      h={small ? 12 : 14}
      borderRadius='lg'
      objectFit='cover'
      borderWidth={2}
      borderColor={small ? 'gray.800' : 'gray.700'}
      borderStyle='solid'
      flexShrink={0}
    />
    <Flex direction='column' gap={0.5} flex={1}>
      <Text fontWeight={600} noOfLines={1}>
        {file.name}
      </Text>
      <Flex alignItems='center' gap={4}>
        <Text fontSize='sm' color='gray.500'>
          {formatBytes(file.size)}
        </Text>
        {!noDelete && (
          <Button
            variant='link'
            colorScheme='red'
            onClick={removeFile}
            size='sm'
          >
            Удалить
          </Button>
        )}
      </Flex>
    </Flex>
  </Flex>
)
