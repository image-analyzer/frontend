import { useCallback, useState } from 'react'

import axios from 'axios'
import { toast } from 'sonner'

import { AnalyzeResultDict } from 'types/analyzeResult'

import { Result } from './Result/Result'
import { UploadFiles } from './UploadFiles/UploadFiles'
import { useFiles } from './hooks/useFiles'

export const Analyzer = () => {
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [result, setResult] = useState<AnalyzeResultDict | null>(null)

  const { files, dropzone, removeFile, removeAllFiles } = useFiles()

  const onSubmit = useCallback(async () => {
    const formData = new FormData()
    files.forEach(file => {
      formData.append('image', file)
    })

    setIsLoading(true)

    try {
      const response = await axios.post<AnalyzeResultDict>(
        '/api/analyze_image/',
        formData,
        {
          maxBodyLength: 104857600, // 100mb
          maxContentLength: 104857600 // 100mb
        }
      )
      setResult(response.data)
    } catch (e) {
      console.log(e)
      toast.error(JSON.stringify(e), {
        style: { padding: 16 }
      })
    } finally {
      setIsLoading(false)
    }
  }, [files])

  const reset = useCallback(() => {
    removeAllFiles()
    setResult(null)
  }, [removeAllFiles])

  return result ? (
    <Result result={result} files={files} back={reset} />
  ) : (
    <UploadFiles
      files={files}
      dropzone={dropzone}
      removeFile={removeFile}
      onSubmit={onSubmit}
      isLoading={isLoading}
    />
  )
}
