import { useCallback, useState } from 'react'

import { useDropzone } from 'react-dropzone'
import { toast } from 'sonner'

export const useFiles = () => {
  const [files, setFiles] = useState<File[]>([])

  const dropzone = useDropzone({
    accept: {
      'image/jpeg': ['.jpg', '.jpeg'],
      'image/png': ['.png'],
      'image/heic': ['.heic']
    },
    noClick: true,
    onDropAccepted: files => {
      if (files) {
        setFiles(prev => [...prev, ...files])
      }
    },
    onDropRejected: files => {
      if (
        files.some(file =>
          file.errors.some(err => err.code === 'file-invalid-type')
        )
      ) {
        toast.error('Некоторые файлы не были загружены', {
          description:
            'Поддерживаемые форматы файлов: .png, .jpeg, .jpg, .heic',
          style: { padding: 16 }
        })
      }
    },
    multiple: true
  })

  const removeFile = useCallback(
    (index: number) => {
      const filtered = files.filter((_, i) => i !== index)
      setFiles(filtered)
    },
    [files]
  )

  const removeAllFiles = useCallback(() => {
    setFiles([])
  }, [])

  return {
    files,
    dropzone,
    removeFile,
    removeAllFiles
  }
}
