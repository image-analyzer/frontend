import { Button, Flex, Text } from '@chakra-ui/react'

import { DropzoneState } from 'react-dropzone'

import { FilePreview } from 'components/FilePreview/FilePreview'
import { ArrowRightIcon } from 'components/Icons/ArrowRightIcon'

import { NoFilesLabel } from './NoFilesLabel/NoFilesLabel'

interface UploadFilesProps {
  files: File[]
  removeFile: (index: number) => void
  onSubmit: () => void
  dropzone: DropzoneState
  isLoading: boolean
}

export const UploadFiles = ({
  files,
  removeFile,
  onSubmit,
  dropzone,
  isLoading
}: UploadFilesProps) => {
  const { getRootProps, getInputProps, open, isDragActive } = dropzone

  return (
    <Flex direction='column' alignItems='flex-end' gap={3}>
      <Flex
        w='100%'
        px={5}
        py={8}
        borderWidth={1}
        borderStyle='dashed'
        borderColor={isDragActive ? 'gray.400' : 'gray.600'}
        borderRadius='xl'
        bg={isDragActive ? 'gray.700' : 'gray.900'}
        transition='all .15s ease-in-out'
        {...getRootProps()}
      >
        <input {...getInputProps()} style={{ display: 'none' }} />
        {files.length ? (
          <Flex direction='column' gap={4}>
            {files.map((file, index) => (
              <FilePreview
                key={file.name}
                file={file}
                removeFile={() => removeFile(index)}
              />
            ))}
            <Flex gap={1} alignItems='center' flexWrap='wrap'>
              <Button variant='link' colorScheme='blue' onClick={open}>
                + добавить еще
              </Button>
              <Text color='gray.400'>или перетащите в эту зону</Text>
            </Flex>
          </Flex>
        ) : (
          <NoFilesLabel isDragActive={isDragActive} open={open} />
        )}
      </Flex>
      {!!files.length && (
        <Button
          colorScheme='blue'
          rightIcon={<ArrowRightIcon boxSize={5} />}
          onClick={onSubmit}
          isLoading={isLoading}
        >
          Исследовать
        </Button>
      )}
    </Flex>
  )
}
