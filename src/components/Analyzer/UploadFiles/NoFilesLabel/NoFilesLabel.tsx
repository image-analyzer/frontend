import { Button, Flex, Text } from '@chakra-ui/react'

import { UploadIcon } from 'components/Icons/UploadIcon'

interface NoFilesLabelProps {
  isDragActive: boolean
  open: () => void
}

export const NoFilesLabel = ({ isDragActive, open }: NoFilesLabelProps) => (
  <Flex
    w='100%'
    direction='column'
    alignItems='center'
    justifyContent='center'
    textAlign='center'
    gap={4}
  >
    <UploadIcon
      boxSize={20}
      color='gray.600'
      transform={isDragActive ? 'translateY(-5px)' : 'translateY(0)'}
      transition='transform .25s ease-in-out'
    />
    <Flex direction='column'>
      <Text fontWeight={600}>Перетащите файлы в эту зону или</Text>
      <Button variant='link' colorScheme='blue' onClick={open}>
        выберите файлы
      </Button>
    </Flex>
    <Text color='gray.500'>
      Поддерживаемые форматы файлов:{' '}
      <Text as='span' color='gray.300' fontWeight={500}>
        .png, .jpeg, .jpg, .heic
      </Text>
    </Text>
  </Flex>
)
