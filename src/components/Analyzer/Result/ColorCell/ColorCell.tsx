import { CloseIcon } from '@chakra-ui/icons'
import { Flex } from '@chakra-ui/react'

interface ColorCellProps {
  color: [number, number, number, number?]
}

export const ColorCell = ({ color: colorArray }: ColorCellProps) => {
  const [red, green, blue, alpha] = colorArray
  const isRGBA = alpha !== undefined
  const alphaRatio = isRGBA ? alpha / 255 : 255
  const color = isRGBA
    ? `rgba(${red}, ${green}, ${blue}, ${alphaRatio})`
    : `rgb(${colorArray.join(',')})`
  const isTransparent = alphaRatio === 0

  return (
    <Flex
      justifyContent='center'
      alignItems='center'
      bg={color}
      borderWidth={2}
      borderColor='gray.800'
      borderStyle='solid'
      w={10}
      h={10}
      borderRadius='lg'
    >
      {isTransparent && <CloseIcon color='red.700' boxSize={3} />}
    </Flex>
  )
}
