import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Flex,
  Image,
  Stack,
  Text
} from '@chakra-ui/react'

import { Field } from 'components/Field/Field'
import { FilePreview } from 'components/FilePreview/FilePreview'
import { ArrowLeftIcon } from 'components/Icons/ArrowLeftIcon'

import { AnalyzeResultDict } from 'types/analyzeResult'

import { ColorCell } from './ColorCell/ColorCell'

interface ResultProps {
  result: AnalyzeResultDict
  files: File[]
  back: () => void
}

export const Result = ({ result, files, back }: ResultProps) => (
  <Flex direction='column' alignItems='flex-start' gap={10}>
    <Button
      variant='link'
      size='lg'
      leftIcon={<ArrowLeftIcon boxSize={5} />}
      onClick={back}
    >
      Назад к загрузке
    </Button>

    <Stack w='100%' spacing={8}>
      {Object.values(result).map((item, index) => {
        const file = files[index]
        return (
          <Card key={`${Math.random()}`} borderRadius='xl'>
            <CardHeader borderBottomWidth={1}>
              <FilePreview file={file} small noDelete />
            </CardHeader>
            <CardBody>
              <Stack spacing={4}>
                <Field label='Исходное изображение'>
                  <Image
                    src={`/${item.preview_path}`}
                    alt={file.name}
                    w={40}
                    h='auto'
                    borderRadius='xl'
                  />
                </Field>
                <Field label='Популярные цвета'>
                  <Flex flexWrap='wrap' gap={2}>
                    {item.popular_colors.map(color => (
                      <ColorCell key={`${Math.random()}`} color={color[0]} />
                    ))}
                  </Flex>
                </Field>
                <Field label='Похожие цвета'>
                  <Flex flexWrap='wrap' gap={2}>
                    {item.similar_colors.map(color => (
                      <ColorCell key={`${Math.random()}`} color={color} />
                    ))}
                  </Flex>
                </Field>
                <Field label='Информация об изображении'>
                  <Flex direction='column' gap={1}>
                    {Object.entries(item.metadata).map(([key, value]) => (
                      <Flex key={key} alignItems='baseline' gap={2}>
                        <Text fontSize='sm' color='gray.400' fontWeight={600}>
                          {key}:
                        </Text>
                        <Text color='gray.100'>{value}</Text>
                      </Flex>
                    ))}
                  </Flex>
                </Field>
                <Field label='Гистограмма'>
                  <Image
                    src={`/${item.histogram_path}`}
                    alt='Гистограмма'
                    w='100%'
                    maxW='500px'
                    h='auto'
                    borderRadius='xl'
                  />
                </Field>
              </Stack>
            </CardBody>
          </Card>
        )
      })}
    </Stack>
  </Flex>
)
