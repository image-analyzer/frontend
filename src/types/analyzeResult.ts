import { SimilarColor } from 'types/similarColor'

import { PopularColor } from './popularColor'

export interface AnalyzeResult {
  histogram_path: string
  preview_path: string
  popular_colors: PopularColor[]
  similar_colors: SimilarColor[]
  metadata: Record<string, string>
}

export type AnalyzeResultDict = Record<string, AnalyzeResult>
