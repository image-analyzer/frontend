import { Analyzer } from 'components/Analyzer/Analyzer'
import { Layout } from 'layouts/Layout'
import { Providers } from 'providers/Providers'
import 'styles/global.css'

import 'assets/fonts/nunito/stylesheet.css'

export const App = () => (
  <Providers>
    <Layout>
      <Analyzer />
    </Layout>
  </Providers>
)
