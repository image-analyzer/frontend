import { ReactNode } from 'react'

import { Toaster } from 'sonner'

import { ChakraProvider } from './chakra/ChakraProvider'

interface ProvidersProps {
  children: ReactNode
}

export const Providers = ({ children }: ProvidersProps) => (
  <ChakraProvider>
    <Toaster richColors />
    {children}
  </ChakraProvider>
)
