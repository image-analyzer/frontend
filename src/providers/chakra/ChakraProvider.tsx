import { ReactNode } from 'react'

import { ChakraProvider as ChakraUiProvider } from '@chakra-ui/react'

import { chakraTheme } from 'styles/chakraTheme'

interface ChakraProviderProps {
  children: ReactNode
}

export const ChakraProvider = ({ children }: ChakraProviderProps) => (
  <ChakraUiProvider theme={chakraTheme}>{children}</ChakraUiProvider>
)
